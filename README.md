# Getting Started 
A set of basic alternate menu screens for Freelancer. Please feel free to use these in your mod with credit, or use them as the basis for your own custom menu screens!

Please note that you should be able to use this mod with vanilla Freelancer, as well as with multiplayer without issue. It only changes the menu screens.

## INSTALLATION

1. Download and install Freelancer Mod Manager 2.0 from [here](https://the-starport.net/freelancer/download/visit.php?cid=1&lid=2706)
2. Download [alt-menu-pack.flmod](https://gitlab.com/IrateRedKite/freelancer-alternate-menu-screen-pack/raw/current/alt-menu-pack.flmod) and open it.
3. Enable the Alternate Menu Screen Pack in Freelancer Mod Manager

## MULTIPLAYER

On many modern systems, Freelancer will not generate a multiplayer ID correctly. In order to fix this you will need to do the following.

1. Close Freelancer, if you have it open.
2. Run FLMPIDGEN.exe
3. Change the output format to 'Registry Datei (.reg)'
4. Click generate, and save the file somewhere you can find it. It doesn't matter what you call this.
5. Double click the file, and allow Windows to add the values to your registry. You should now have a functional multiplayer key

## CONTRIBUTING AND REUSING

Please refer to the license file for information on re-using these screens. If you use them as they are in your mod, credit is appreciated. Note that these are intended as simple templates and teaching tools for modders looking to modify their own menu screens, so please feel free to use them as the basis for your own scripts!

